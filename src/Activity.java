import java.util.Arrays;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;

public class Activity {
    public static void main(String[] args) {
        /* A C T I V I T Y
        * 1. Create a new Java project in your S2 folder named a1
        * 2 Create an array of the following fruits and print it in the console:
        *   a. apple
        *   b. avocado
        *   c. banana
        *   d. kiwi
        *   e. orange
        * 3. Create a new instance of the Scanner class and ask the user which fruit
        *       they would like the index of.
        * 4. Print out a message informing the user of the index number of the fruit they
        *       are looking for.
        * 5. Create an ArrayList of String data-type elements and add 4 of your friends name into it
        * 6. Output the contents of the ArrayList concatenated with a string message in the console
        * 7. Create a HashMap of keys with data type String and values with data type integer
        * 8. Output the contents of the HashMap in the Terminal
        * */

        //Answer:
        //1-4
        String[] arrayList = new String[5];

        arrayList[0] = "Apple";
        arrayList[1] = "Avocado";
        arrayList[2] = "Banana";
        arrayList[3] = "Kiwi";
        arrayList[4] = "Orange";

        System.out.println("Fruits in stock: " + Arrays.toString(arrayList));
        System.out.println("Choose a fruit  you like to get its index " );

        //binarySearch() method
        Scanner scannerFruit = new Scanner(System.in);
        String fruit = scannerFruit.nextLine();
        int result = Arrays.binarySearch(arrayList,fruit);
        System.out.println("The index of " + fruit + " is: " + result);

        //5-6
        String[] friendList = new String[4];

        friendList[0] = "Tony";
        friendList[1] = "Steve";
        friendList[2] = "John";
        friendList[3] = "Peter";
        System.out.println("My MCU friends are: " + Arrays.toString(friendList));

        //7-8
        Map<String, Integer> Prutas = new HashMap<>();
        Prutas.put("Avocado", 53);
        Prutas.put("Banana", 100);
        Prutas.put("Kiwi", 23);
        Prutas.put("Orange", 15);
        System.out.println(Prutas);

    }


}
